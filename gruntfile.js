
module.exports = function (grunt) {
    grunt.initConfig({
        critical: {
            dist: {
                options: {
                    base: './'
                },
                // The source file
                src: 'demo/home-m.html',
                // The destination file
                dest: 'demo/home-m-tn.html'
            }
        }
    });
    // Load the plugins
    grunt.loadNpmTasks('grunt-critical');

    // Default tasks.
    grunt.registerTask('default', ['critical']);
};