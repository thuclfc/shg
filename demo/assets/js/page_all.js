/*!
 * project_shg
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2020. MIT licensed.
 */$(document).ready(function () {
    tabs('.game_tab li', '.game_list_shg', 0, 0);
    tabs('.news_tab li', '.news_content', 0, 0);
    box_tab('.guide_tab li', '.guide_content', 0, 0);

    $('.guide_menu a').on('click', function () {
        let id_answer = $(this).attr('href');
        $('html,body').animate({ scrollTop: $(id_answer).offset().top - 52 }, 500);
    });

    //active menu current site
    var urlcurrent = window.location.href;
    $(".menu li a[href$='" + urlcurrent + "']").addClass('active');

    //show menu mobile
    $('.navbar-toggler,.btn_login').on('click', function () {
        $('.navbar-collapse,.navbar-modal').toggleClass('show');
    });
    $('.navbar-collapse .close,.navbar-modal').on('click', function () {
        $('.navbar-collapse,.navbar-modal').removeClass('show');
    });



    $(window).on('load',function () {
        var fb_large = '<iframe class="large" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fsohagame%2F&tabs=timeline&width=500&height=435&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="500" height="435" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>';
        var fb_small = '<iframe class="small" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fsohagame%2F&tabs=timeline&width=350&height=435&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="350" height="435" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>';
        var size_window = $(window).width();
        var fontex = '<link href="https://fonts.googleapis.com/css?family=Exo+2:300,400,500,600,700,900&display=swap" rel="stylesheet">';
        $('.topgame').css('height','auto');
        $('body').addClass('is-f');
        $('.topgame_list').css('opacity','1');
        $('.skeleton1').addClass('hidden');
        /* $('.game_slider .slick-slide').addClass('show');*/
        /* $('.skeleton').addClass('hidden');*/
        $('.game_slider .content,.game_slider .game_info').css('opacity','1');
        $('head').append(fontex);

        if(size_window > 1680){
            $('.bottom .fanpage').append(fb_large);
        }else{
            $('.bottom .fanpage').append(fb_small);
        }
    });
});