/*!
 * project_shg
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2020. MIT licensed.
 */$(document).ready(function () {
    tabs('.newsgame_tab li', '.newsgame_content', 0, 0);
    $('.game_slider').slick({
        centerMode: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        centerPadding: 0,
        dots: false,
        fade: true,
        arrows: true,
        responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false,
                dots: true
            }
        }]
    });
    $('.topgame_list').slick({
        centerMode: false,
        slidesToShow: 6,
        slidesToScroll: 1,
        infinite: false,
        centerPadding: 0,
        dots: false,
        arrows: true,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 2
            }
        }]
    });
});